from HCR_preprocessing import saveDataToBin
from HCR_preprocessing import loadDataBin
import pandas as pd
import gc

gc.enable()

train = loadDataBin("bin/input/train_good")
test  = loadDataBin("bin/input/test_good")

importance = pd.read_csv("output/feature_importance.csv", usecols = ["FEATURES", "SPLIT", "GAIN"])
bad_features = importance["FEATURES"].unique().tolist()

train.drop(columns = bad_features, axis = 1, inplace = True)
test.drop(columns = bad_features, axis = 1, inplace = True)

saveDataToBin(train, "train_good", "input")
saveDataToBin(test, "test_good", "input")

del train, test, importance, bad_features; gc.collect()
