import matplotlib.pyplot as plot
import seaborn as sns
import pandas as pd
import numpy as np
import gc
import os

# plot missing values
def plot_missing(train, test):

    path = "plots/missing"

    if not os.path.exists(path):
        os.makedirs(path)

    train_miss = pd.DataFrame(train.isnull().sum()/train.shape[0] * 100).reset_index()
    test_miss = pd.DataFrame(test.isnull().sum()/test.shape[0] * 100).reset_index()
    train_miss["TYPE"] = "Train"
    test_miss["TYPE"] = "Test"

    # creating a dataframe tha contain the values missing in both datasets
    missing = pd.concat([train_miss, test_miss], axis=0).rename(index = str, columns={"index": "Feature", 0:"AMT"})

    fig = plt.figure(figsize=(20, 10))
    ax = sns.pointplot("Feature", "AMT", data=missing, hue="TYPE")
    plt.xticks(rotation=90, fontsize=8)
    plt.title("Missing Data (%)")
    plt.xlabel("Feature")
    plt.ylabel("Percentage")
    ax.set_facecolor("k")
    fig.set_facecolor("lightgrey")
    fig.savefig(path + "/missing_data.png")
    plt.close(fig)

    del train_miss, test_miss, missing; gc.collect()

# function to plot featuress
def plot_(df, feature):

    if not os.path.exists("plots"):
        os.makedirs("plots")

    fig = plt.figure(figsize=(15, 10))
    fig.set_facecolor("lightgrey")
    rows = df.shape[0]

    if df[feature].dtype == "object": #plot to categorical features
        path = "plots/categorical_features"
        if not os.path.exists(path):
            os.makedirs(path)

        feat_df = pd.DataFrame(df[feature].value_counts()/rows * 100).reset_index().rename(index=str, columns={"index": "Class", feature: "Percentage"})
        ax = sns.barplot(x = "Class", y ="Percentage", data=feat_df)
        plt.xticks(rotation=45, fontsize=10)
        plt.title("Percentage of Class in Feature {}".format(feature))
        plt.xlabel("Class")
        plt.ylabel("Percentage")
        fig.savefig("{}/{}_class.png".format(path, feature))
        plt.close(fig)
        del feat_df; gc.collect()

    else: # plot to numerical features

        path = "plots/numeric_features"
        if not os.path.exists(path):
            os.makedirs(path)

        # pa
        if df[feature].isnull().any():
            df_ = df[df[feature].isnull() == False].copy()
        else:
            df_ = df.copy()

        if feature not in ["TARGET", "SK_ID_CURR"]:
            ax = sns.distplot(df_[feature])
            plt.title("{} Distribution".format(feature))
            fig.savefig("{}/{}_distribution.png".format(path, feature))
            plt.close(fig)

        del df_; gc.collect()
