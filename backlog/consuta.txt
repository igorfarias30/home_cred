-- dim empresa
select emp.emp_codemp,
		count(emp.emp_codemp) as qtd_empresa_emp,
		fat.fat_empresa,
		count(fat.fat_empresa) as qtd_empresa_fat
		from empresa as emp right join fatura as fat on emp.emp_codemp = fat.fat_empresa
			group by 1, 3
			order by 4 asc;

-- dim cliente
-- Aqui possuem alguns clientes que n�o est�o mapeados na tabela pessoa
select 
	pes.pes_codpes,
	count(pes.pes_codemp) as qtd_cli_pessoas,
	fat.fat_codpes,
	count(fat.fat_codpes) as qtd_cli_fat
	from pessoa as pes right join fatura as fat on pes.pes_codpes = fat.fat_codpes
		group by 1, 3
		order by 4, 2 asc;

-- dim item (produto)
select 
	itf.ift_numfat,
	count(itf.ift_numfat) as qtd_fatura_tbitem,
	fat.fat_numfat,
	count(fat.fat_numfat) as qtd_fatura_tbfatura
	from item_fatura as itf right join fatura as fat on itf.ift_numfat = fat.fat_numfat
		group by 1, 3
		order by 4, 2 asc;

select 
	item.itm_item,
	count(item.itm_item) as qtd_item_tbitem,
	itf.ift_item as qtd_item,
	count(itf.ift_item) as qtd_item_tbitem_fatura 
	from item right join item_fatura as itf on item.itm_item = itf.ift_item 
		group by 1, 3
		order by 4, 2 asc;
		
-- fato vendas --
select 
	itf.ift_empresa,
	(select empresa.emp_fantasia from empresa where empresa.emp_codemp = itf.ift_empresa ) as nome_empresa,
	itf.ift_loja,
	itf.ift_numfat,
	(select pessoa.pes_codpes from pessoa where pessoa.pes_codpes = (select fatura.fat_codpes from fatura where fatura.fat_numfat = itf.ift_numfat)  ) as cod_cliente,
	(select fatura.fat_nomcli from fatura where fatura.fat_numfat = itf.ift_numfat and fatura.fat_empresa = itf.ift_empresa) as cliente,
	(select fatura.fat_baient from fatura where fatura.fat_numfat = itf.ift_numfat and fatura.fat_empresa = itf.ift_empresa) as cliente_bairro,
	itf.ift_item,
	itf.ift_usuario,
	itf.ift_desitm,
	itf.ift_valor,
	itf.ift_quantfatc,
	(itf.ift_valor * itf.ift_quantfatc) as valor,
	(select fatura.fat_datfat from fatura where fatura.fat_numfat = itf.ift_numfat and fatura.fat_empresa = itf.ift_empresa) as data_entrada
	from item_fatura as itf where itf.ift_numfat = '00000538'
		order by data_entrada