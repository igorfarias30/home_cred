from sklearn.model_selection import train_test_split, StratifiedKFold, learning_curve, GridSearchCV
from sklearn.metrics import accuracy_score, roc_auc_score, confusion_matrix
from HCR_preprocessing import loadDataBin, print_dataframe
#from sklearn.feature_selection import VarianceThreshold
from sklearn.model_selection import KFold
from lightgbm import LGBMClassifier
import matplotlib.pyplot as plt
import lightgbm as lgbm
import numpy as np
import time
import gc

gc.enable()

data = loadDataBin("bin/input/train_")
test = loadDataBin("bin/input/test_")

y = data["TARGET"].astype(np.int32)
data.drop("TARGET", axis = 1, inplace = True)
test.drop("TARGET", axis = 1, inplace = True)

# applying int
data['SK_ID_CURR'] = data["SK_ID_CURR"].apply(lambda value: int(value))
test['SK_ID_CURR'] = test["SK_ID_CURR"].apply(lambda value: int(value))

#create a validation set
X_train, X_test, y_train, y_test = train_test_split(data.iloc[:, :], y, test_size = 0.3, stratify = y)

# number of folds to apply
folds = StratifiedKFold(n_splits = 5, shuffle = True, random_state = round(time.time()) + 10000)

oof_preds = np.zeros(X_train.shape[0])
loc_preds = np.zeros(X_test.shape[0])
sub_preds = np.zeros(test.shape[0])
feats = [f for f in data.columns if f not in ['SK_ID_CURR']]

start = time.time()
for n_fold, (trn_idx, val_idx) in enumerate(folds.split(X_train, y_train)):

    trn_x, trn_y = X_train.iloc[trn_idx, 1:], y_train.iloc[trn_idx]
    val_x, val_y = X_train.iloc[val_idx, 1:], y_train.iloc[val_idx]

    clf = LGBMClassifier(
        n_estimators = 10000, #4000
        learning_rate = 0.02, #0.03
        num_leaves = 32,
        colsample_bytree = .9497036,
        subsample = .9,
        max_depth = 8,
        reg_alpha = .04,
        reg_lambda = .073,
        min_split_gain =.0222415,
        nthread = -1,
        min_child_weight = 40,
        silent = -1,
        verbose = -1,
        is_unbalance = True
    )

    clf.fit(trn_x, trn_y,
            eval_set= [(trn_x, trn_y), (val_x, val_y)],
            eval_metric='auc',
            verbose=200,
            early_stopping_rounds=100  #30
           )

    oof_preds[val_idx] = clf.predict_proba(val_x, num_iteration = clf.best_iteration_)[:, 1]
    loc_preds += clf.predict_proba(X_test.iloc[:, 1:], num_iteration = clf.best_iteration_)[:, 1] / folds.n_splits # local validation
    sub_preds += clf.predict_proba(test[feats], num_iteration = clf.best_iteration_)[:, 1] / folds.n_splits # test data to submit

    #print("Fold {} AUC: {:.6f}".format(n_fold + 1, roc_auc_score(val_y, oof_preds[val_idx])))

print("Time CV: {:.4f} seconds".format(time.time() - start))
#print("Full AUC Score train: {:.6f}".format(roc_auc_score(y_train, oof_preds)))
print("Full AUC Score local validation: {:.6f}".format(roc_auc_score(y_test, loc_preds)))

test['TARGET'] = sub_preds
test[['SK_ID_CURR', 'TARGET']].to_csv('output/newBuroF_submission.csv', index=False)

del data, test, y, sub_preds, oof_preds, X_train, X_test, y_train, y_test, trn_x, trn_y, val_x, val_y; gc.collect()

"""
def plot_learning_curve(estimator, title, X, y, ylim = None, cv = None, n_jobs = -1, train_sizes = np.linspace(0.1, 1.0, 5)):

    plt.figure()
    plt.title(title)
    if ylim is not None:
        plt.ylim(*ylim)
    plt.xlabel("Training examples")
    plt.ylabel("Score")

    train_sizes, train_scores, test_scores, = learning_curve(estimator, X, y, cv=cv, n_jobs = n_jobs, train_sizes = train_sizes)
    train_scores_mean  = np.mean(train_scores, axis = 1)
    train_scores_std = np.std(train_scores, axis = 1)
    test_scores_mean = np.mean(test_scores, axis = 1)
    test_scores_std = np.std(test_scores, axis = 1)
    plt.grid()

    plt.fill_between(train_sizes, train_scores_mean - train_scores_std, train_scores_mean + train_scores_std, alpha = 0.1, color = "r")
    plt.fill_between(train_sizes, test_scores_mean - test_scores_std, test_scores_mean + test_scores_std, alpha = 0.1, color = "g")
    plt.plot(train_sizes, train_scores_mean, 'o-', color = "r", label = "Training score")
    plt.plot(train_sizes, test_scores_mean, 'o-', color = "g", label = "Cross-validation score")
    plt.legend(loc = "best")
    return plt

kfold = StratifiedKFold(n_splits = 2)
# plot chart for model

fig = plt.figure(figsize = (20, 15))
g = plot_learning_curve(lgb_cv.best_estimator_, "LightGBM learning curves", X_train, y_train, cv = kfold)
g.show()
"""
