from sklearn.model_selection import train_test_split
from HCR_preprocessing import loadDataBin
import matplotlib.pyplot as plt
import lightgbm as lgbm
import pandas as pd
import numpy as np
import time
import gc

gc.enable()

print("Loading data ...")

# reading train and test datasets
train = loadDataBin("bin/input/train_")
test = loadDataBin("bin/input/test_")

y = train["TARGET"].astype(np.int32)
train.drop("TARGET", axis = 1, inplace = True)
test.drop("TARGET", axis = 1, inplace = True)

# convert sk_id to int
train['SK_ID_CURR'] = train["SK_ID_CURR"].apply(lambda value: int(value))
test['SK_ID_CURR'] = test["SK_ID_CURR"].apply(lambda value: int(value))

#train = train[["SK_ID_CURR", "EXT_1_MULT", "EXT_2_MULT", "EXT_3_MULT", "AMT_ANNUITY", "EXT_1_ADDI", "EXT_2_ADDI", "EXT_3_ADDI"]]
#  test = test[["SK_ID_CURR", "EXT_1_MULT", "EXT_2_MULT", "EXT_3_MULT", "AMT_ANNUITY", "EXT_1_ADDI", "EXT_2_ADDI", "EXT_3_ADDI"]]

# train and test n_splits
X_train, X_test, y_train, y_test = train_test_split(train.iloc[:, 1:], y, stratify = y, test_size = 0.25)

# features
features = [feat for feat in train.columns if feat != 'SK_ID_CURR']

# form lightgbm datasets
train_lgb = lgbm.Dataset(data = X_train, label = y_train)
test_lgb =  lgbm.Dataset(data = X_test, label = y_test, reference = train_lgb)

# parameters
lgbm_params = {
        "boosting": 'gbdt',
        #"boosting": 'dart',
        #"boosting": 'goss',
        "learning_rate": 0.01, #0.03
        "num_leaves": 70,
        "max_depth": 7,
        "objective": 'binary',
        "colsample_bytree": .8,
        "subsample": .9,
        "reg_alpha": .1,
        "reg_lambda": .1,
        "min_split_gain": .005,
        #"n_jobs": -1,
        "min_child_weight": 2,
        #"silent": -1,
        "verbose": -1,
        "is_unbalance": True
    }

start = time.time()

#LightGBM Cross validation
cv = lgbm.cv(params = lgbm_params,
            train_set = train_lgb,
            num_boost_round = 4000,
            nfold = 5,
            stratified = True,
            early_stopping_rounds = 50,
            verbose_eval = 100,
            show_stdv = True,
            metrics = ['auc']
            #seed = round(time.time()) + 100
        )

print("Time CV: {:.4f} seconds".format( (time.time() - start) ))

# optimum boost
optimum_boost_rounds = np.argmax(cv['auc-mean']) + 1
print("Optimum boost rounds = {}".format(optimum_boost_rounds))
print("Best CV result: {}".format(np.max(cv['auc-mean'])))
print("Mean CV result: {}".format(np.mean(cv['auc-mean'])))

# train model
model = lgbm.train( train_set = train_lgb,
                    params = lgbm_params,
                    num_boost_round = optimum_boost_rounds,
                    valid_sets = test_lgb,
                    verbose_eval = 0)

y_pred = model.predict(test[features])

fig, (ax1, ax2) = plt.subplots(1, 2, figsize = [20, 10])
try:
    lgbm.plot_importance(model, ax = ax1, max_num_features = 20, importance_type = 'split')
    lgbm.plot_importance(model, ax = ax2, max_num_features = 20, importance_type = 'gain')
except:
    pass
ax1.set_title("Splits - Importance")
ax2.set_title("Gain - Importance")
plt.tight_layout()
plt.savefig("plots/models/feature_importance_2.png")

try:
    ax = lgbm.plot_tree(model, tree_index = 50, figsize=(20,  10), show_info=['split_gain'])
    plt.show()
except Exception as e:
    print("Error: ", e)

# saving features importance in dataframe
importance = pd.DataFrame({"FEATURES": model.feature_name(),
                            "SPLIT": model.feature_importance(importance_type = "split"),
                            "GAIN": model.feature_importance(importance_type = "gain")}
                            ).sort_values(["SPLIT", "GAIN"], ascending = False)

importance =  importance[(importance["SPLIT"] == 0) & (importance["GAIN"] == 0)].copy()
importance.to_csv("output/feature_importance.csv")

del train, test, y, X_train, X_test, y_train, y_test, features, train_lgb, test_lgb, lgbm_params, cv, optimum_boost_rounds, model, y_pred, importance; gc.collect()
#https://github.com/shep312/kaggle-scripts/blob/master/titanic/notebooks/titantic_classification.ipynb
