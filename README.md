# HOME CREDIT DEFAULT RISK

Repositório do código fonte da competição no Kaggle. O objetivo é aprender mais sobre a área de Data Science, ter experiências em competições no Kaggle e preencher tempos ociosos na nossa rotina (new hobby :D ).

## Can you predict how capable each applicant is of repaying a loan?
Premiação: $70,000

![relacionamento](plots/relationship.png)

Para ver a página da competição, clique neste [link](https://www.kaggle.com/c/home-credit-default-risk).

## Pré-requisitos
* Anaconda 3
* LightGBM

### application
* This is the main table, broken into two files for Train (with TARGET) and Test (without TARGET).
* Static data for all applications. One row represents one loan in our data sample.

#### columns
* SK_ID_CURR: ID of loan in our sample
* NAME_CONTRACT_TYPE: Identification if loan is cash or revolving
* CODE_GENDER: Gender of the client
* FLAG_OWN_CAR: Flag if the client owns a car
* FLAG_OWN_REALTY: Flag if client owns a house or flat
* CNT_CHILDREN: Number of children the client has
* AMT_INCOME_TOTAL: Income of the client
* AMT_CREDIT: Credit amount of the loan
* AMT_ANNUITY: Loan annuity
* AMT_GOODS_PRICE: For consumer loans it is the price of the goods for which the loan is given
* NAME_TYPE_SUITE: Who was accompanying client when he was applying for the loan
* NAME_INCOME_TYPE: Clients income type (businessman, working, maternity leave)
* NAME_EDUCATION_TYPE: Level of highest education the client achieved
* NAME_FAMILY_STATUS: Family status of the client
* NAME_HOUSING_TYPE: What is the housing situation of the client (renting, living with parents, ...)
* REGION_POPULATION_RELATIVE: Normalized population of region where client lives (higher number means the client lives in more populated region)
* DAYS_BIRTH: Client's age in days at the time of application, time only relative to the application
* DAYS_EMPLOYED: How many days before the application the person started current employment, time only relative to the application
DAYS_REGISTRATION: How many days before the application did client change his registration, time only relative to the application
DAYS_ID_PUBLISH: How many days before the application did client change the identity document with which he applied for the loan, time only relative to the application
* OWN_CAR_AGE: Age of client's car
* FLAG_MOBIL: Did client provide mobile phone (1=YES, 0=NO)
* FLAG_EMP_PHONE: Did client provide work phone (1=YES, 0=NO)
* FLAG_WORK_PHONE: Did client provide home phone (1=YES, 0=NO)
* FLAG_CONT_MOBILE: Was mobile phone reachable (1=YES, 0=NO)
* FLAG_PHONE: Did client provide home phone (1=YES, 0=NO)
* FLAG_EMAIL: Did client provide email (1=YES, 0=NO)
* OCCUPATION_TYPE: What kind of occupation does the client have
* REGION_RATING_CLIENT: Our rating of the region where client lives (1,2,3)
* REGION_RATING_CLIENT_W_CITY: Our rating of the region where client lives with taking city into account (1,2,3)
* WEEKDAY_APPR_PROCESS_START: On which day of the week did the client apply for the loan
* HOUR_APPR_PROCESS_START: Approximately at what hour did the client apply for the loan
* REG_REGION_NOT_LIVE_REGION: Flag if client's permanent address does not match contact address (1=different, 0=same, at region level)
* REG_REGION_NOT_WORK_REGION: Flag if client's permanent address does not match work address (1=different, 0=same, at region level)
* LIVE_REGION_NOT_WORK_REGION: Flag if client's contact address does not match work address (1=different, 0=same, at region level)
* REG_CITY_NOT_LIVE_CITY: Flag if client's permanent address does not match contact address (1=different, 0=same, at city level)
* REG_CITY_NOT_WORK_CITY: Flag if client's permanent address does not match work address (1=different, 0=same, at city level)
* LIVE_CITY_NOT_WORK_CITY: Flag if client's contact address does not match work address (1=different, 0=same, at city level)
* ORGANIZATION_TYPE: Type of organization where client works
* EXT_SOURCE_1: Normalized score from external data source
* EXT_SOURCE_2: Normalized score from external data source
* EXT_SOURCE_3: Normalized score from external data source
* APARTMENTS_AVG, BASEMENTAREA_AVG, YEARS_BEGINEXPLUATATION_AVG, YEARS_BUILD_AVG, COMMONAREA_AVG, ELEVATORS_AVG, ENTRANCES_AVG, FLOORSMAX_AVG, FLOORSMIN_AVG, LANDAREA_AVG, LIVINGAPARTMENTS_AVG, LIVINGAREA_AVG, NONLIVINGAPARTMENTS_AVG, NONLIVINGAREA_AVG, APARTMENTS_MODE, BASEMENTAREA_MODE, YEARS_BEGINEXPLUATATION_MODE, YEARS_BUILD_MODE, COMMONAREA_MODE, ELEVATORS_MODE, ENTRANCES_MODE, FLOORSMAX_MODE, FLOORSMIN_MODE, LANDAREA_MODE, LIVINGAPARTMENTS_MODE, LIVINGAREA_MODE, NONLIVINGAPARTMENTS_MODE, NONLIVINGAREA_MODE, APARTMENTS_MEDI, BASEMENTAREA_MEDI, YEARS_BEGINEXPLUATATION_MEDI, YEARS_BUILD_MEDI, COMMONAREA_MEDI, ELEVATORS_MEDI, ENTRANCES_MEDI, FLOORSMAX_MEDI, FLOORSMIN_MEDI, LANDAREA_MEDI, LIVINGAPARTMENTS_MEDI, LIVINGAREA_MEDI, NONLIVINGAPARTMENTS_MEDI, NONLIVINGAREA_MEDI, FONDKAPREMONT_MODE, HOUSETYPE_MODE, TOTALAREA_MODE, WALLSMATERIAL_MODE, EMERGENCYSTATE_MODE: Normalized information about building where the client lives, What is average (_AVG suffix), modus (_MODE suffix), median (_MEDI suffix) apartment size, common area, living area, age of building, number of elevators, number of entrances, state of the building, number of floor
* OBS_30_CNT_SOCIAL_CIRCLE: How many observation of client's social surroundings with observable 30 DPD (days past due) default
* DEF_30_CNT_SOCIAL_CIRCLE: How many observation of client's social surroundings defaulted on 30 DPD (days past due)
* OBS_60_CNT_SOCIAL_CIRCLE: How many observation of client's social surroundings with observable 60 DPD (days past due) default
* DEF_60_CNT_SOCIAL_CIRCLE: How many observation of client's social surroundings defaulted on 60 (days past due) DPD
* DAYS_LAST_PHONE_CHANGE: How many days before application did client change phone
* FLAG_DOCUMENT_2: Did client provide document 2
* FLAG_DOCUMENT_3: Did client provide document 3
* FLAG_DOCUMENT_4: Did client provide document 4
* FLAG_DOCUMENT_5: Did client provide document 5
* FLAG_DOCUMENT_6: Did client provide document 6
* FLAG_DOCUMENT_7: Did client provide document 7
* FLAG_DOCUMENT_8: Did client provide document 8
* FLAG_DOCUMENT_9: Did client provide document 9
* FLAG_DOCUMENT_10: Did client provide document 10
* FLAG_DOCUMENT_11: Did client provide document 11
* FLAG_DOCUMENT_12: Did client provide document 12
* FLAG_DOCUMENT_13: Did client provide document 13
* FLAG_DOCUMENT_14: Did client provide document 14
* FLAG_DOCUMENT_15: Did client provide document 15
* FLAG_DOCUMENT_16: Did client provide document 16
* FLAG_DOCUMENT_17: Did client provide document 17
* FLAG_DOCUMENT_18: Did client provide document 18
* FLAG_DOCUMENT_19: Did client provide document 19
* FLAG_DOCUMENT_20: Did client provide document 20
* FLAG_DOCUMENT_21: Did client provide document 21
* AMT_REQ_CREDIT_BUREAU_HOUR: Number of enquiries to Credit Bureau about the client one hour before application
AMT_REQ_CREDIT_BUREAU_DAY: Number of enquiries to Credit Bureau about the client one day before application (excluding one hour before application)
AMT_REQ_CREDIT_BUREAU_WEEK: Number of enquiries to Credit Bureau about the client one week before application (excluding one day before application)
MT_REQ_CREDIT_BUREAU_MON: Number of enquiries to Credit Bureau about the client one month before application (excluding one week before application)
AMT_REQ_CREDIT_BUREAU_QRT: Number of enquiries to Credit Bureau about the client 3 month before application (excluding one month before application)
* AMT_REQ_CREDIT_BUREAU_YEAR: Number of enquiries to Credit Bureau about the client one day year (excluding last 3 months before application)

* TARGET: Target variable (1 - client with payment difficulties: he/she had late payment more than X days on at least one of the first Y installments of the loan in our sample, 0 - all other cases)

Para instalar o LightGBM:
>$: `pip install lightgbm`
