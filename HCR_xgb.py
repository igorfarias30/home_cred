from sklearn.model_selection import train_test_split, StratifiedKFold, RandomizedSearchCV, learning_curve, GridSearchCV
from sklearn.metrics import roc_auc_score, accuracy_score
from xgboost import XGBClassifier
from HCR_main import loadDataBin
import matplotlib.pyplot as plt
import numpy as np
import time
import gc

# garbage collector
gc.enable()

# reading data
data = loadDataBin("bin/input/train_")
y = data["TARGET"].apply(lambda y: int(y))

# dropping target and convert SK_ID_CURR to int
data.drop("TARGET", axis = 1, inplace = True)
data["SK_ID_CURR"] = data["SK_ID_CURR"].apply(lambda x: int(x))

# applying train_test and split
X_train, X_test, y_train, y_test = train_test_split(data.iloc[:, 1:], y, test_size = 0.25, random_state = round(time.time()))

# setup the parameters and distributions to sample from: param_dist
param_dist = { "learning_rate": [.05],
                "max_depth": np.arange(1, 3), # default 2
                "n_estimators": [100],
                "booster": ['gbtree', 'gblinear', 'dart'],
                "seed": [round(time.time() + 100)],
                "n_jobs": [-1]
        }

# Instantiate Classifier
xgb = XGBClassifier(silent = True)

# intstantiate the RandomizedSearchCv object: tree_cvssaas
#xgb_cv = RandomizedSearchCV(xgb, param_dist, cv = 3)
xgb_cv = GridSearchCV(xgb, param_dist, cv = 2)

# Fit model_selection
xgb_cv.fit(X_train, y_train)

# Make predictions
y_pred = xgb_cv.predict(X_test)

# print results
print("XGBoost parameters:\t{}".format(xgb_cv.best_params_))
print("Best score:\t{}".format(xgb_cv.best_score_))
print("Accuracy score:\t{}".format(round(accuracy_score(y_pred, y_test) * 100, 2)))
#print("Roc Auc Score:\t{}".format(roc_auc_score(y_test, xgb.predict_proba(X_test, num_interation = best_iteration_))))

def plot_learning_curve(estimator, title, X, y, ylim = None, cv = None, n_jobs = -1, train_sizes = np.linspace(0.1, 1.0, 5)):

    plt.figure()
    plt.title(title)
    if ylim is not None:
        plt.ylim(*ylim)
    plt.xlabel("Training examples")
    plt.ylabel("Score")

    train_sizes, train_scores, test_scores, = learning_curve(estimator, X, y, cv=cv, n_jobs = n_jobs, train_sizes = train_sizes)
    train_scores_mean  = np.mean(train_scores, axis = 1)
    train_scores_std = np.std(train_scores, axis = 1)
    test_scores_mean = np.mean(test_scores, axis = 1)
    test_scores_std = np.std(test_scores, axis = 1)
    plt.grid()

    plt.fill_between(train_sizes, train_scores_mean - train_scores_std, train_scores_mean + train_scores_std, alpha = 0.1, color = "r")
    plt.fill_between(train_sizes, test_scores_mean - test_scores_std, test_scores_mean + test_scores_std, alpha = 0.1, color = "g")
    plt.plot(train_sizes, train_scores_mean, 'o-', color = "r", label = "Training score")
    plt.plot(train_sizes, train_scores_mean, 'o-', color = "g", label = "Cross-validation score")
    plt.legend(loc = "best")
    return plt

kfold = StratifiedKFold(n_splits = 2)

#fig = plt.figure(figsize = (20, 15))
g = plot_learning_curve(xgb_cv, "LightGBM learning curves", X_train, y_train, cv = kfold)
g.show()

# del all variables
del data, y, X_train, X_test, y_train, y_test, xgb, xgb_cv, y_pred, kfold; gc.collect()
