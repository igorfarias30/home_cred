from sklearn.preprocessing import LabelEncoder, OneHotEncoder
import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd
import numpy as np
import warnings
import pickle
import gzip
import gc
import os

# Path to dataset
DATA_PATH = "dataset/"

#waning settings
warnings.simplefilter(action = 'ignore', category = FutureWarning)

# setting options to show columns in a dataframe
pd.options.display.max_columns = 100

# garbage collector settings
gc.set_debug(gc.DEBUG_SAVEALL)
gc.enable()

# load pickle object
def loadDataBin(filename):
    with gzip.open(filename, 'r') as file:
        return pickle.load(file)

# save the object to a bin to read file faster
def saveDataToBin(dataframe, filename, type):

    if not os.path.exists("bin"):
        os.makedirs("bin")

    path = "bin/" + type
    if not os.path.exists(path):
        os.makedirs(path)

    with gzip.open(path + "/" + filename, 'wb') as file:
        pickle.dump(dataframe, file, protocol=pickle.HIGHEST_PROTOCOL)

# return a list of interger features
def getInteger(dataframe):
    return [col for col in dataframe.columns if dataframe[col].dtype == 'int64']

# return a list of continous features
def getFloat(dataframe):
    return [col for col in dataframe.columns if dataframe[col].dtype == 'float64']

# get a list of categorical features
def getCategoric(dataframe):
    return [col for col in dataframe.columns if dataframe[col].dtype == 'object']
    #dataframe[categorical_features] = dataframe.select_dtypes(include=["object"]).apply(lambda x: x.astype('object'))

# function to print missing data to any features
def missingData(df, features):

    print("Feature\t\t|\tMissing(%)")
    print("---"*20, "\n")

    for feature in features:
        if df[feature].isnull().any():
            print("{}\t\t{:.2f}%\t\t".format(feature, df[feature].isnull().sum()/df.shape[0] * 100))

    print("\n")

# Remove features without correlation
def remove_bad_features(data, y, features):

    # remove features in (-0.09, 0.09)
    for feat in features:
        index = data[~(data[feat].isnull())][feat].index
        if (data.loc[index, feat].corr(y[index]) < 0.009) and (data.loc[index, feat].corr(y) > - 0.009):
            #data.drop(feat, axis = 1, inplace = True)
            print("removing feature: {}".format(feat))

    return data

# applying pre processing in payments csv
def pre_payments(payments):

    #payments.sort_values(by = ["SK_ID_CURR", "SK_ID_PREV", "NUM_INSTALMENT_NUMBER"], ascending = True).head(30)
    #payments.head(10)
    #payments.groupby(["SK_ID_CURR", "SK_ID_PREV", "BOOLEAN_ATRASO"]).size().reset_index().groupby("SK_ID_CURR").size()
    #payments.groupby(["SK_ID_PREV"]).size().reset_index() # amount of application of a client
    #payments[payments["SK_ID_PREV"] == 1330831].sort_values(by = 'DAYS_INSTALMENT').head(40)

    # payments.groupby(["SK_ID_CURR", "SK_ID_PREV", "BOOLEAN_ATRASO"])["PAYMENT_AMT_DIFF"].std().reset_index()

    # BOOLEAN_ATRASO verifica se o cliente atrasou uma parcela do empréstimo
    payments["BOOLEAN_ATRASO"] = (payments["DAYS_INSTALMENT"] < payments["DAYS_ENTRY_PAYMENT"]).map({False:0, True:1})

    # verificando a quantidade de dias que ela atrasou o pagamento
    def pay_em_dias(pagamento, vencimento):
        if pagamento <= vencimento:
            return pagamento - vencimento
        else:
            return vencimento - pagamento

    payments["PAYMENT_DAYS_DIFF"] = payments[["DAYS_INSTALMENT", "DAYS_ENTRY_PAYMENT"]].apply(lambda values: pay_em_dias(*values), axis = 1) # esta feature calcula a quantidade de dias de atraso de uma parcela do financeamento
    #payments.groupby("SK_ID_CURR").size().sort_values(ascending = False).head(10) # quantidade

    payments["PAYMENT_AMT_PERC"] = payments["AMT_PAYMENT"] / payments["AMT_INSTALMENT"]
    payments["PAYMENT_AMT_DIFF"] = payments["AMT_INSTALMENT"] - payments["AMT_PAYMENT"]
    aggregations = {
            #"NUM_INSTALMENT_VERSION": ['unique'],
            "AMT_INSTALMENT": ['min', 'max', 'mean', 'sum'],
            "AMT_PAYMENT": ['min', 'max', 'mean', 'sum'],
            "PAYMENT_AMT_PERC": ['min', 'max', 'mean', 'sum'],
            "PAYMENT_AMT_DIFF": ['min', 'max', 'mean', 'sum']
            #"PAYMENT_DAY_DIFF": ['min', 'max', 'mean', 'sum']
    }

    # aggregations by SK_ID_CURR
    payments_agg = payments.groupby("SK_ID_CURR").agg(aggregations)
    payments_agg.columns = pd.Index(["PAYMENTS_" + feat[0] + "_" + feat[1].upper() for feat in payments_agg.columns.tolist() ])
    payments_agg["PAYMENTS_APPLI"] = payments.groupby(["SK_ID_CURR", "SK_ID_PREV"]).size().reset_index().groupby("SK_ID_CURR").size()

    payments_agg["PAYMENTS_DAYS_MEAN_ATRASO"] = payments[payments["BOOLEAN_ATRASO"] == 1].groupby("SK_ID_CURR")["PAYMENT_DAYS_DIFF"].mean() # adquirindo a média de dias de um cliente que atrasou
    payments_agg["PAYMENTS_DAYS_MEAN_EMDIAS"] = payments[payments["BOOLEAN_ATRASO"] == 0].groupby("SK_ID_CURR")["PAYMENT_DAYS_DIFF"].mean() # adquirindo a média de dias de um cliente que pagou em dias
    payments_agg["PAYMENTS_DAYS_AMNT_ATRASO"] = payments[payments["BOOLEAN_ATRASO"] == 1].groupby("SK_ID_CURR")["PAYMENT_DAYS_DIFF"].count() # quantidade de atrasos que um cliente já teve
    payments_agg["PAYMENTS_DAYS_AMNT_EMDIAS"] = payments[payments["BOOLEAN_ATRASO"] == 0].groupby("SK_ID_CURR")["PAYMENT_DAYS_DIFF"].count() # quantidade de pagamentos em dia de um cliente
    payments_agg["PAYMENTS_MNEY_DIFF_ATRASO"] = payments[payments["BOOLEAN_ATRASO"] == 1].groupby("SK_ID_CURR")["PAYMENT_AMT_DIFF"].mean()

    # calculando a taxa de atraso de um cliente
    payments_agg["ATRASO_RATE_PER_ID"] = payments[payments["BOOLEAN_ATRASO"] == 1].groupby("SK_ID_CURR")["PAYMENT_DAYS_DIFF"].count()/ \
                (payments[payments["BOOLEAN_ATRASO"] == 1].groupby("SK_ID_CURR")["PAYMENT_DAYS_DIFF"].count() + \
                payments[payments["BOOLEAN_ATRASO"] == 0].groupby("SK_ID_CURR")["PAYMENT_DAYS_DIFF"].count())

    # calculando a taxa de pagamentos em dias de um cliente
    payments_agg["PGDIAS_RATE_PER_ID"] = payments[payments["BOOLEAN_ATRASO"] == 0].groupby("SK_ID_CURR")["PAYMENT_DAYS_DIFF"].count()/ \
                (payments[payments["BOOLEAN_ATRASO"] == 1].groupby("SK_ID_CURR")["PAYMENT_DAYS_DIFF"].count() + \
                payments[payments["BOOLEAN_ATRASO"] == 0].groupby("SK_ID_CURR")["PAYMENT_DAYS_DIFF"].count())

    payments_agg.reset_index(inplace = True)

    # payments_agg.head()
    return payments_agg

# pre_processing buerau balance
def pre_bureau_balance(bureau_balance):

    bureau_bal_aux = pd.DataFrame(bureau_balance["SK_ID_BUREAU"].unique(), columns = {"SK_ID_BUREAU"})
    bureau_bal_aux["PREVIOS_BALANCE_COUNT"] = bureau_balance.groupby("SK_ID_BUREAU", as_index = True)["SK_ID_BUREAU"].count().values
    colunas = pd.crosstab(bureau_balance['SK_ID_BUREAU'], bureau_balance['STATUS']).reset_index(drop=True).columns
    colunas = ['STATUS_' + col for col in colunas ] #renaming the columns
    bureau_bal_aux[colunas] = pd.crosstab(bureau_balance['SK_ID_BUREAU'], bureau_balance['STATUS']).reset_index(drop=True)

    # new features to month
    bureau_bal_aux["MONTHS_MIN"]  = bureau_balance.groupby("SK_ID_BUREAU", as_index = True)["MONTHS_BALANCE"].min().values
    bureau_bal_aux["MONTHS_MAX"]  = bureau_balance.groupby("SK_ID_BUREAU", as_index = True)["MONTHS_BALANCE"].max().values
    bureau_bal_aux["MONTHS_MEAN"] = bureau_balance.groupby("SK_ID_BUREAU", as_index = True)["MONTHS_BALANCE"].mean().values
    bureau_bal_aux["MONTHS_STD"]  = bureau_balance.groupby("SK_ID_BUREAU", as_index = True)["MONTHS_BALANCE"].std().values
    # bureau_bal_aux[~(bureau_bal_aux["PREVIOS_BALANCE_COUNT"].isnull())]
    #most_recent_index = bureau_balance.groupby("SK_ID_BUREAU")["MONTHS_BALANCE"].idxmax()

    return bureau_bal_aux

# pre_processing in bureau dataset and after applying a merge in data
def pre_bureau(bureau, bureau_balance):

    # preprocessing in bureau
    bureau["PCT_CREDIT_SUM_DEBT"] = bureau["AMT_CREDIT_SUM_DEBT"]/bureau["AMT_CREDIT_SUM"]
    bureau["PCT_CREDIT_SUM_LIMIT"] = bureau["AMT_CREDIT_SUM_DEBT"]/bureau["AMT_CREDIT_SUM_LIMIT"]
    bureau["PCT_CREDIT_SUM_OVERDUE"] = bureau["AMT_CREDIT_SUM_OVERDUE"]/bureau["AMT_CREDIT_SUM"]
    bureau["PCT_CREDIT_SUM_OVERDUE_LIMIT"] = bureau["AMT_CREDIT_SUM_OVERDUE"]/bureau["AMT_CREDIT_SUM_LIMIT"]
    bureau['PCT_CREDIT_MAX_OVERDUE'] = bureau['AMT_CREDIT_MAX_OVERDUE']/bureau['AMT_CREDIT_SUM']
    bureau['PCT_CREDIT_MAX_OVERDUE_LIMIT'] = bureau['AMT_CREDIT_MAX_OVERDUE']/bureau['AMT_CREDIT_SUM_LIMIT']
    bureau['PCT_ANNUITY'] = bureau['AMT_ANNUITY']/bureau['AMT_CREDIT_SUM']

    # calling pre_processing in bureau_balance
    bureau_balance = pre_bureau_balance(bureau_balance)
    colunas_balance = [col for col in bureau_balance.columns if col != "SK_ID_BUREAU"] # get the columns in pre_processing bureau_balance
    bureau = bureau.merge(right = bureau_balance, how = "left", on = "SK_ID_BUREAU")

    # bureau features to apply in bureau_aux
    features_bureau = ["PCT_CREDIT_SUM_DEBT", "PCT_CREDIT_SUM_LIMIT", "PCT_CREDIT_SUM_OVERDUE",
                        "PCT_CREDIT_SUM_OVERDUE_LIMIT", "PCT_CREDIT_MAX_OVERDUE", "PCT_CREDIT_MAX_OVERDUE_LIMIT", "PCT_ANNUITY",
                        "AMT_CREDIT_SUM_DEBT", "AMT_CREDIT_SUM", "AMT_CREDIT_SUM_LIMIT", "AMT_CREDIT_MAX_OVERDUE"]

    # preprocessing
    bureau_aux = pd.DataFrame(bureau["SK_ID_CURR"].unique(), columns = {"SK_ID_CURR"})
    bureau_aux["PREVIOUS_LOANS_COUNT"] = bureau.groupby("SK_ID_CURR", as_index = False)["SK_ID_BUREAU"].count()["SK_ID_BUREAU"]
    bureau_aux[['ACTIVE_COUNT', 'BAD_DEBT_COUNT', 'CLOSED_COUNT', 'SOLD_COUNT']] = (pd.crosstab(bureau['SK_ID_CURR'], bureau['CREDIT_ACTIVE']).reset_index(drop=True))
    bureau_aux[['ACTIVE_AMT_CREDIT_MEAN', 'BAD_AMT_CREDIT_MEAN', 'CLOSED_AMT_CREDIT_MEAN', 'SOLD_AMT_CREDIT_MEAN']] = pd.pivot_table(bureau, values = "AMT_CREDIT_SUM", index = ['SK_ID_CURR'], columns = ['CREDIT_ACTIVE'], aggfunc = np.mean, fill_value = 0).reset_index(drop = True)
    colunas = pd.crosstab(bureau['SK_ID_CURR'], bureau['CREDIT_TYPE']).reset_index(drop=True).columns
    bureau_aux[colunas] = pd.crosstab(bureau['SK_ID_CURR'], bureau['CREDIT_TYPE']).reset_index(drop=True)
    bureau_aux[features_bureau] = bureau.groupby("SK_ID_CURR", as_index = False)[features_bureau].mean()[features_bureau]
    bureau_aux[colunas_balance] = bureau.groupby("SK_ID_CURR", as_index = False)[colunas_balance].mean()[colunas_balance]

    return bureau_aux

# pre_processing data
def pre_processing(df, features_cat, features_num, y, strategy = None):

    rows = df.shape[0]

    if strategy == 'replace':
        for feature in features_cat:
            classes = df[feature].unique()
            top_ = df[feature].describe()["top"]
            for cla_ in classes:
                if (df[df[feature] == cla_].shape[0]/rows * 100) < 0.005: # removing rows with classes are not so expressivily
                    index_ = df[df[feature] == cla_][feature].index
                    df.loc[index_, feature] = top_
                    #print("Replacing class {} in feature {} to {}".format(cla_, feature, top_))
                    del index_; gc.collect()

            del classes, top_; gc.collect()

            if df[feature].isnull().any(): # replacing in features categorics the top value
                if (df[feature].isnull().sum()/df.shape[0] * 100) < 0.5:
                    index_ = df[df[feature].isnull() == True][feature].index
                    top_ = df[feature].describe()["top"]
                    df.loc[index_, feature] = top_
                    del index_, top_; gc.collect()

                else:
                    ''' Combining variables to estimate the value of missing data
                        The heuristic consisting in:
                        combining many categorical features, the value that most repeat is the value that i have to put in missing data,
                        but combining the same variable
                    '''

                    for contrat in df["NAME_CONTRACT_TYPE"].unique():
                        for sex in df["CODE_GENDER"].unique():
                            for car in df["FLAG_OWN_CAR"].unique():
                                for realty in df["FLAG_OWN_REALTY"].unique():

                                    # passing the most commum value into dataframe
                                    df_tmp = df[(df["NAME_CONTRACT_TYPE"] == contrat) & (df["CODE_GENDER"] == sex) & (df["FLAG_OWN_CAR"] == car) & (df["FLAG_OWN_REALTY"] == realty)].copy()

                                    size = df_tmp[feature].value_counts().shape[0]
                                    if size > 0:
                                        value = df_tmp[feature].describe()["top"] # value most commum with feature contrat + sex + car + realty
                                        index_null = df_tmp[df_tmp[feature].isnull() == True][feature].index # feature index tha contain missing data
                                        df.loc[index_null, feature] = value
                                        del df_tmp, index_null, value; gc.collect()

        # strategy to missing data
        if strategy == "drop":
            df.dropna(axis=0, how="any", inplace=True)

        else:

            # When a cliente doesn't have a car, OWN_CAR_AGE is NaN
            index_ = df[df["FLAG_OWN_CAR"] == 'N']["FLAG_OWN_CAR"].index
            df.loc[index_, "OWN_CAR_AGE"] = -99
            df.drop("FLAG_OWN_CAR", axis = 1, inplace = True)

            # pre-processing in numeric features
            for feature in features_num:
                amt_null = df[feature].isnull().sum()/df.shape[0] * 100
                correla_ = df[feature].corr(y)
                if (amt_null > 50) and ((correla_ < 0.1) and (correla_ > - 0.1)):
                    #print("Dropping feature...{}".format(feature))
                    df.drop([feature], axis=1, inplace=True)

                else:
                    #print("Replacing missing data in feature {} to average of the feature".format(feature))
                    mean_, std_, ind_ = df[feature].mean(), df[feature].std(), df[df[feature].isnull() == True][feature].index
                    df.loc[ind_, feature] = np.random.normal(mean_, std_, 1)[0]
                    del mean_, ind_; gc.collect()

    # new features to model
    df["EXT_1_MULT"] = df["EXT_SOURCE_1"] * df["EXT_SOURCE_2"] * df["EXT_SOURCE_3"]
    df["EXT_2_MULT"] = df["EXT_SOURCE_1"] * df["EXT_SOURCE_2"]
    df["EXT_3_MULT"] = df["EXT_SOURCE_1"] * df["EXT_SOURCE_3"]
    df["EXT_4_MULT"] = df["EXT_SOURCE_2"] * df["EXT_SOURCE_3"]
    df["EXT_1_ADDI"] = df["EXT_SOURCE_1"] + df["EXT_SOURCE_2"] + df["EXT_SOURCE_3"]
    df["EXT_2_ADDI"] = df["EXT_SOURCE_1"] + df["EXT_SOURCE_2"]
    df["EXT_3_ADDI"] = df["EXT_SOURCE_1"] + df["EXT_SOURCE_3"]
    df["EXT_4_ADDI"] = df["EXT_SOURCE_2"] + df["EXT_SOURCE_3"]
    df['EXT_SOURCES_MEAN'] = df[['EXT_SOURCE_1', 'EXT_SOURCE_2', 'EXT_SOURCE_3']].mean(axis=1)
    df['EXT_SOURCES_STD'] = df[['EXT_SOURCE_1', 'EXT_SOURCE_2', 'EXT_SOURCE_3']].std(axis=1)
    #df['NEW_SCORES_STD'] = df['NEW_SCORES_STD'].fillna(df['NEW_SCORES_STD'].mean())

    df["ANNUITY_LENGTH_NEW"] = df["AMT_CREDIT"]/df["AMT_ANNUITY"]
    df["OVER_EXPECT_CREDIT_NEW"] = (df["AMT_CREDIT"] > df["AMT_GOODS_PRICE"]).map({False:0, True:1})
    df["CREDIT_TO_GOODS_RATIO_NEW"] = df["AMT_CREDIT"] / df["AMT_GOODS_PRICE"]
    df["AMT_INCOME_PER_CHILDREN_NEW"] = df["AMT_INCOME_TOTAL"]/df["CNT_CHILDREN"]
    df["WORKING_LIFE_RATIO_NEW"] = df["DAYS_EMPLOYED"]/df["DAYS_BIRTH"]
    df["ANNUITY_TO_INCOME_RATIO_NEW"] = df['AMT_ANNUITY'] / (df['AMT_INCOME_TOTAL'])
    df["INCOME_CREDIT_PERC_NEW"] = df["AMT_INCOME_TOTAL"]/df["AMT_CREDIT"]
    df["INCOME_PER_FAM_NEW"] = df["AMT_INCOME_TOTAL"]/df["CNT_FAM_MEMBERS"]
    df["CHILDREN_RATIO_NEW"] = df["CNT_CHILDREN"]/df["CNT_FAM_MEMBERS"]
    # https://www.kaggle.com/sz8416/eda-baseline-model-using-application

    return df

#print the shape of the dataframe
def print_dataframe(dataframe):
    print("Dataframe with {} rows | {} columns".format(dataframe.shape[0], dataframe.shape[1]))

# main function
def main():

    # if bin directory already exists, so load bin files
    if os.path.exists("bin/") and (len(os.listdir('bin/')) > 0):
        train = loadDataBin("bin/dataframe/" + "application_train")
        test = loadDataBin("bin/dataframe/" + "application_test")
        bureau = loadDataBin("bin/dataframe/" + "bureau")
        bureau_balance = loadDataBin("bin/dataframe/" + "bureau_balance")
        payments = loadDataBin("bin/dataframe/" + "installments_payments")
        #previous = loadDataBin("bin/data/frame" + "previous_application")

        print("Binaries objects has been read!\n")
    else:
        os.makedirs("bin/")
        train = pd.read_csv(DATA_PATH + "application_train.csv")
        test = pd.read_csv(DATA_PATH + "application_test.csv")
        bureau = pd.read_csv(DATA_PATH + "bureau.csv")
        balance = pd.read_csv(DATA_PATH + "bureau_balance.csv")
        payments = pd.read_csv(DATA_PATH + "installments_payments.csv")
        pos_cash = pd.read_csv(DATA_PATH + "POS_CASH_balance.csv")
        previous = pd.read_csv(DATA_PATH + "previous_application.csv")

        # saved bin object in a pickled file
        saveDataToBin(train, "application_train", 'dataframe')
        saveDataToBin(test, "application_test", "dataframe")
        saveDataToBin(bureau, "bureau", 'dataframe')
        saveDataToBin(balance, "bureau_balance", "dataframe")
        saveDataToBin(payments, "installments_payments", 'dataframe')
        saveDataToBin(pos_cash, "POS_CASH_balance", "dataframe")
        saveDataToBin(previous, "previous_application", "dataframe")

    index = train.shape[0] # get the index where start test dataset
    data = pd.concat([train, test], axis = 0, sort=False).reset_index() # concatenate the two dataset in only dataset
    data.drop("index", axis=1, inplace=True)

    y = data["TARGET"]
    data.drop("TARGET", axis=1, inplace=True)

    # merging bureau data
    bureau = pre_bureau(bureau, bureau_balance)
    data = data.merge(right = bureau, how = 'left', on = 'SK_ID_CURR')
    print("Merge Bureau has done!\n")

    # merging bureau data
    payments = pre_payments(payments)
    data = data.merge(right = payments, how = 'left', on = 'SK_ID_CURR')
    print("Merge Payments has done!\n")

    feat_cat = getCategoric(data) # get features with categorical data
    feat_num = getInteger(data)   # get features with integer dadta
    feat_flt = getFloat(data)     # get features with float data

    data = pre_processing(data, feat_cat, feat_num + feat_flt, y)

    # apply dummies in vaiables
    data = pd.get_dummies(data,  drop_first = True, dtype = np.int8).reset_index()
    data.drop("index", axis=1, inplace=True)
    data.rename(columns = lambda col: col.replace(' ', '_'), inplace = True) # replace features with spaces by '_'

    # get target
    y_train = pd.DataFrame(y[:index], columns = ["TARGET"])
    y_test  = pd.DataFrame(y[index:], columns = ["TARGET"])

    # saving dataframe in a binarie file
    print("Saving data to bin!")
    saveDataToBin(pd.concat([data.loc[:index - 1, :], y_train], axis=1, sort=False), "train_", "input")
    saveDataToBin(pd.concat([data.loc[index:, :], y_test],  axis=1, sort=False), "test_", "input")

    del train, test, bureau, bureau_balance, payments, feat_cat, feat_num, feat_flt, data, y_train, y_test, y; gc.collect()

if __name__ == "__main__":
     main()
     import sys
     # https://tryolabs.com/blog/2017/03/16/pandas-seaborn-a-guide-to-handle-visualize-data-elegantly/ -> visualizations
     # https://www.kaggle.com/kanncaa1/statistical-learning-tutorial-for-beginners -> statical learning (plots, cumulative distributions)
     # https://www.kaggle.com/davidcoxon/titanic-practice-by-davidcoxon -> titanic analysis
     # https://www.kaggle.com/slundberg/interpreting-a-lightgbm-model -> lgbm model
     # https://github.com/slundberg/shap -> shap analysis to lgbm
     # https://www.kaggle.com/shep312/deep-learning-in-tf-with-upsampling-lb-742 -> roc acu score analysis
     sys.modules[__name__].__dict__.clear()
